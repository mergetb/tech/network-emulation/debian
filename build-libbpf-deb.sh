#!/bin/bash

set -e

function usage() {
  echo "usage: build-libbpf-deb.sh [options]"
  echo "options:"
  echo "  -c: build in a container"
  exit 1
}

while getopts "ch" opt; do
    case "$opt" in
    h)
      usage
      ;;
    c)
      containerize=true
      ;;
    esac
done

if [[ $containerize ]]; then
  docker run -v `pwd`:/code -w /code mergetb/kernel-builder /code/build-libbpf-deb.sh
  exit $?
fi

rm -f libbpf*.build*
rm -f libbpf*.changes
rm -f libbpf*.deb

debuild -e V=1 -e prefix=/usr $DEBBUILD_ARGS -i -us -uc -b

mv ../libbpf*.build* .
mv ../libbpf*.changes .
mv ../libbpf*.deb .

