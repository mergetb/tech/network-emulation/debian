#!/bin/bash

set -e

function usage() {
  echo "usage: build-kernel-deb.sh [options]"
  echo "options:"
  echo "  -c: build in a container"
  exit 1
}

while getopts "ch" opt; do
    case "$opt" in
    h)
      usage
      ;;
    c)
      containerize=true
      ;;
    esac
done

if [[ $containerize ]]; then
  docker run -v `pwd`:/code -w /code mergetb/kernel-builder /code/build-kernel-deb.sh
  exit $?
fi

cp kconfig kernel/.config
cd kernel
make deb-pkg -j`nproc`
